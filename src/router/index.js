import ListProducts from '../views/ListProducts.vue'
import DetailProduct from '../views/DetailProduct.vue'
import RequestAttention from '../views/RequestAttention.vue';
import FollowUs from '../views/FollowUs.vue';
import RequestSong from '../views/RequestSong.vue';

import VueRouter from 'vue-router'

const routes = [
  {
    path: '/client/list-products',
    name: 'Client ListProducts',
    component: ListProducts
  },
  {
    path: '/client/list-products/:id',
    name: 'Client ListProducts Detail',
    component: DetailProduct
  },
  {
    path: '/client/request-attention',
    name: 'Client Request Attention',
    component: RequestAttention
  },
  {
    path: '/client/follow-us',
    name: 'Client Follow Us',
    component: FollowUs
  },
  {
    path: '/client/request-song',
    name: 'Client Request Song',
    component: RequestSong
  }
]

if(process.env.VUE_APP_MODE == 'development') {
  routes.unshift({
    path: '/',
    redirect: '/client/list-products'
  });
}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export  {
  router,
  routes
}
