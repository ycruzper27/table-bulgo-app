export default [
    {
        channelId: "UCXstu08N_roVNdSXOV3Cdyw",
        channelTitle: "Song Lyrics",
        description: "Como Estrellas - La Young.",
        idVideo: "LnDWb0vY8nw",
        liveBroadcastContent: "none",
        publishTime: "2020-07-22T15:20:12Z",
        publishedAt: "2020-07-22T15:20:12Z",
        highlight: false,
        title: 'La Young - Como Estrellas (Letra)',
        thumbnails: {
            default: {
                height: 90,
                url: "https://i.ytimg.com/vi/LnDWb0vY8nw/default.jpg",
                width: 120
            },
            high: {
                height: 360,
                url: "https://i.ytimg.com/vi/LnDWb0vY8nw/hqdefault.jpg",
                width: 480
            },
            medium: {
                height: 180,
                url: "https://i.ytimg.com/vi/LnDWb0vY8nw/mqdefault.jpg",
                width: 320
            }
        }
    },
    {
        "channelId": "UCyInsLK0iu7xYvKmOKIZe8w",
        "channelTitle": "Zion & Lennox",
        "description": "Zion & Lennox - Estrella (OFFICIAL VIDEO) SUBSCRIBETE a Nuestro Canal para toda nuestra musica exclusiva▷ https://bit.ly/zionlennoxYT Escucha ...",
        "idVideo": "Ni-nD40TZoo",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/Ni-nD40TZoo/default.jpg",
            "width": 120,
            "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/Ni-nD40TZoo/mqdefault.jpg",
            "width": 320,
            "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/Ni-nD40TZoo/hqdefault.jpg",
            "width": 480,
            "height": 360
          }
        },
        "title": "Zion &amp; Lennox - Estrella (OFFICIAL VIDEO)",
        "liveBroadcastContent": "none",
        "publishTime": "2021-07-22T23:00:03Z",
        highlight: true
    },
]