export default [
    {
        coverPage: 'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
        name: 'Hamburgesa potente',
        images: [
            'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
        ],
        rating: 4,
        description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius corporis veniam obcaecati architecto eum ipsam blanditiis sdddsa dasde,d ede...',
        price: 19000,
        highlight: true
    },
    {
        coverPage: 'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
        name: 'Hamburgesa potente',
        images: [
            'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
        ],
        rating: 3,
        description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius corporis veniam obcaecati architecto eum ipsam blanditiis sdddsa dasde,d ede...',
        price: 13000,
        highlight: true
    },
    {
        coverPage: 'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
        name: 'Hamburgesa potente',
        images: [
            'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
        ],
        rating: 5,
        description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius corporis veniam obcaecati architecto eum ipsam blanditiis sdddsa dasde,d ede...',
        price: 35000,
        highlight: true
    },
    {
        coverPage: 'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
        name: 'Hamburgesa potente',
        images: [
            'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
        ],
        rating: 5,
        description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius corporis veniam obcaecati architecto eum ipsam blanditiis sdddsa dasde,d ede...',
        price: 119000,
        highlight: true
    },
    {
        coverPage: 'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
        name: 'Hamburgesa potente',
        images: [
            'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
            'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
        ],
        rating: 4,
        description: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius corporis veniam obcaecati architecto eum ipsam blanditiis sdddsa dasde,d ede...',
        price: 190000,
        highlight: true
    },
]