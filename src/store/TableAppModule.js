
import MenuIcon from 'bulgo-spa-library/src/assets/icons/menu_icon.svg?inline'
import BellIcon from 'bulgo-spa-library/src/assets/icons/bell_icon.svg?inline'
import InstagramIcon from 'bulgo-spa-library/src/assets/icons/instagram_icon.svg?inline'
import MusicIcon from 'bulgo-spa-library/src/assets/icons/music_icon.svg?inline'
import ToyIcon from 'bulgo-spa-library/src/assets/icons/toy_icon.svg?inline'

const state = {
    buttonsBottomNavigation: [
        { icon: MenuIcon, key: 'list-products', active: true, type: 'router', meta: '/client/list-products'},
        { icon: BellIcon, key: 'request-attention', active: false, type: 'router', meta: '/client/request-attention'},
        { icon: InstagramIcon, key: 'follow-us', active: false, type: 'router', meta: '/client/follow-us'},
        { icon: MusicIcon, key: 'request-song', active: false, type: 'router', meta: '/client/request-song'},
        { icon: ToyIcon, key: 'list-toys', active: false, type: 'router', meta: '/client/list-toys'},
    ]
}
const mutations = {
    SET_ACTIVE_BUTTON(state, payload) {
        state.buttonsBottomNavigation[payload.index].active = payload.value;
    }
}
const actions = {
    activeButton({commit, state}, button) {
        state.buttonsBottomNavigation.forEach((item,index) => {
            commit('SET_ACTIVE_BUTTON',
                item.key == button.key ? { index, value:true } : {index, value: false});
        })
    },
    validateRouteActive({ commit, state }){
        const button = state.buttonsBottomNavigation.find(x => ~window.location.pathname.indexOf(x.key));
        // * Just active button BUT not redirect route
        state.buttonsBottomNavigation.forEach((item,index) => {
            commit('SET_ACTIVE_BUTTON',
                item.key == button.key ? { index, value:true } : {index, value: false});
        })
    }
}
const getters = {
    getButtonsBottomNavigation(state){
        return state.TableApp.buttonsBottomNavigation;
    }
}

const module = {
    name: 'TableApp',
    namespaced:true,
    state,
    mutations,
    actions,
}

export {
    module,
    getters
}