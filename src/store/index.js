import Vue from 'vue'
import Vuex from 'vuex'
import { module as TableAppModule, getters as gettersTableApp } from './TableAppModule';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    [TableAppModule.name]: TableAppModule
  },
  getters: {
    ...gettersTableApp
  }
})
