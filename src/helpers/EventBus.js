
const EventBusEvents = {
    changeIconBulgo: 'changeIconBulgo',
    openMenuSearchResults: 'openMenuSearchResults'
}

export {
    EventBusEvents
}