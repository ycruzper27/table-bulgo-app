import {
    BulgoLogo,
    registerFiltersAndHelpersVue,
    ModeApp,
    EventBus, EventBusEvents
} from 'bulgo-spa-library'
import Vuetify from '../plugins/vuetify';
import { EventBusEvents as LocalEventBusEvents } from './EventBus'

export default class Helper extends ModeApp {
    constructor(props) {
        super(props)
        // * Register Filters Vue and helpers $vue
        registerFiltersAndHelpersVue(Vuetify.framework,
            process.env.VUE_APP_MODE == 'development',
            {
                ...process.env,
                EventBusEvents: LocalEventBusEvents
            });
        // this.validActiveNavigationBottom();
    }

    // time_change_night = '18:00:00'

    changeConfigDesktop() {
        this.type_app = 'desktop'
        // this.changePropertyCss({ variable: '--bg-page', value: 'bg_desktop_login' })
        this.toggleOverlayLoader(false);
    }
    changeConfigDay() {
        this.BulgoLogo = BulgoLogo('blue');
        EventBus.$emit(EventBusEvents.changeIconBulgo, this.BulgoLogo);
        localStorage.setItem('@logo', 'blue');
        this.type_app = 'day_app'
        this.changePropertyCss({ variable: '--bg-page', value: 'white_static' });
        this.changePropertyCss({ variable: '--color-font', value: 'gradient_blue' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-border', value: 'blue_static' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-color', value: 'blue_static' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-color-active', value: 'white_static' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-bg', value: 'gradient_blue' });
        // * Input styles
        this.changePropertyCss({ variable: '--placeholder-color', value: 'blue_static' })
        this.changePropertyCss({ variable: '--color-input-text', value: 'blue_static' })
        // * Chip styles
        this.changePropertyCss({ variable: '--chip-outlined-color', value: 'blue_static' })
        this.changePropertyCss({ variable: '--chip-outlined-text', value: 'white_static' })
        this.changePropertyCss({ variable: '--chip-color-fill', value: 'blue_static' })
        // * Card styles
        this.changePropertyCss({ variable: '--card-product-bg', value: 'white_soft' })
        this.changePropertyCss({ variable: '--card-product-text', value: 'gradient_blue' })
        // ? Highlight
        this.changePropertyCss({ variable: '--highlight-color-bg', value: 'gradient_blue' })
        this.changePropertyCss({ variable: '--highlight-color-text', value: 'white_static' })
        // * Rating styles
        this.changePropertyCss({ variable: '--primary-color-rating', value: 'blue_static' });
        // * Add Cart style
        this.changePropertyCss({ variable: '--primary-color-add-cart', value: 'blue_static' });
        // * Badge styles
        this.changePropertyCss({ variable: '--badge-bg', value: 'blue_static' });
        this.changePropertyCss({ variable: '--badge-color', value: 'white_static' });
        // * Link Device styles
        this.changePropertyCss({ variable: '--btn-link-device-bg', value: 'gradient_gold' });
        this.changePropertyCss({ variable: '--btn-link-device-color', value: 'white_static' });
        this.changePropertyCss({ variable: '--btn-link-device-shadow', value: 'gold_static' });
        // * Autocomplete Styles
        this.changePropertyCss({ variable: '--autocomplete-bg', value: 'white_static' });
        this.changePropertyCss({ variable: '--autocomplete-color', value: 'blue_static' });
        this.changePropertyCss({ variable: '--autocomplete-color-second', value: 'gray_bold_static' });
        // * Notification Styles
        this.changePropertyCss({ variable: '--notification-bg', value: 'gradient_blue' });
        this.changePropertyCss({ variable: '--notification-border', value: 'blue_static' });
        this.changePropertyCss({ variable: '--notification-color', value: 'white_static' });
        // * Svg Styles
        this.changePropertyCss({ variable: '--primary-svg-color', value: 'blue_static' });
        // * Card song styles
        this.changePropertyCss({ variable: '--card-song-default-bg', value: 'gray_static' });
        this.changePropertyCss({ variable: '--card-song-default-color', value: 'blue_static' });
        this.changePropertyCss({ variable: '--card-song-highlight-bg', value: 'gradient_blue' });
        this.changePropertyCss({ variable: '--card-song-highlight-color', value: 'white_static' });

        this.toggleOverlayLoader(false);
    }
    changeConfigNight() {
        this.BulgoLogo = BulgoLogo('white');
        EventBus.$emit(EventBusEvents.changeIconBulgo, this.BulgoLogo);
        localStorage.setItem('@logo', 'white');
        this.type_app = 'night_app'
        this.changePropertyCss({ variable: '--bg-page', value: 'gradient_blue' });
        this.changePropertyCss({ variable: '--color-font', value: 'white_static' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-border', value: 'gold_static' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-color', value: 'white_static' });
        this.changePropertyCss({ variable: '--primary-btn-navigation-bg', value: 'gradient_gold' });
        // * Input styles
        this.changePropertyCss({ variable: '--placeholder-color', value: 'white_static' })
        this.changePropertyCss({ variable: '--color-input-text', value: 'white_static' })
        // * Chip styles
        this.changePropertyCss({ variable: '--chip-outlined-color', value: 'white_static' })
        this.changePropertyCss({ variable: '--chip-outlined-text', value: 'white_static' })
        this.changePropertyCss({ variable: '--chip-color-fill', value: 'gradient_gold' })
        // * Card styles
        this.changePropertyCss({ variable: '--card-product-bg', value: 'white_static' });
        this.changePropertyCss({ variable: '--card-product-text', value: 'gradient_blue' })
        // ? Highlight
        this.changePropertyCss({ variable: '--highlight-color-bg', value: 'gradient_gold' })
        this.changePropertyCss({ variable: '--highlight-color-text', value: 'white_static' })
        // * Rating styles
        this.changePropertyCss({ variable: '--primary-color-rating', value: 'gold_static' });
        // * Add Cart style
        this.changePropertyCss({ variable: '--primary-color-add-cart', value: 'gold_static' });
        // * Badge styles
        this.changePropertyCss({ variable: '--badge-bg', value: 'gold_static' });
        this.changePropertyCss({ variable: '--badge-color', value: 'white_static' });
        // * Link Device styles
        this.changePropertyCss({ variable: '--btn-link-device-bg', value: 'white_static' });
        this.changePropertyCss({ variable: '--btn-link-device-color', value: 'blue_static' });
        this.changePropertyCss({ variable: '--btn-link-device-shadow', value: 'white_static' });
        // * Autocomplete Styles
        this.changePropertyCss({ variable: '--autocomplete-bg', value: 'gradient_blue' });
        this.changePropertyCss({ variable: '--autocomplete-color', value: 'white_static' });
        this.changePropertyCss({ variable: '--autocomplete-color-second', value: 'gray_bold_static' });
        // * Notification Styles
        this.changePropertyCss({ variable: '--notification-bg', value: 'gradient_gold' });
        this.changePropertyCss({ variable: '--notification-border', value: 'gold_static' });
        this.changePropertyCss({ variable: '--notification-color', value: 'white_static' });
        // * Svg Styles
        this.changePropertyCss({ variable: '--primary-svg-color', value: 'white_static' });
        // * Card song styles
        this.changePropertyCss({ variable: '--card-song-default-bg', value: 'white_static' });
        this.changePropertyCss({ variable: '--card-song-default-color', value: 'blue_static' });
        this.changePropertyCss({ variable: '--card-song-highlight-bg', value: 'gradient_gold' });
        this.changePropertyCss({ variable: '--card-song-highlight-color', value: 'white_static' });

        this.toggleOverlayLoader(false);
    }
}