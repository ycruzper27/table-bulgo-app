import { routes as routesTableBulgoApp } from "./router";
import { module as TableAppModule, getters as TableAppGetters } from './store/TableAppModule';

export {
    routesTableBulgoApp,
    TableAppModule,
    TableAppGetters
};